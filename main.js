// The items we need to itterate over.
var counters = [
    {
        "label": "Win 10 1709",
        "endDate": "04/09/2019", // Use american notation..! mm/dd/yyyy
        "htmlId": "countdown_1"
    },

    {
        "label": "Win 10 1803",
        "endDate": "11/12/2019", // Use american notation..! mm/dd/yyyy
        "htmlId": "countdown_2"
    },

    {
        "label": "Win 10 1809",
        "endDate": "05/12/2020", // Use american notation..! mm/dd/yyyy
        "htmlId": "countdown_3"
    },

    {
        "label": "Debian 9",
        "endDate": "06/1/2022", // Use american notation..! mm/dd/yyyy
        "htmlId": "countdown_4"
    },

    {
        "label": "Centos 7",
        "endDate": "06/30/2024", // Use american notation..! mm/dd/yyyy
        "htmlId": "countdown_5"
    },
]

// Use this for translating the time labels.
// All are required!
var timeLabels = {
    // key => label to display
    "years": "years",
    "days": "days",
    "hours": "hours",
    "minutes": "minutes",
    "seconds": "seconds",
}

// Call our special object!
Countdown.init(counters, timeLabels);