## Step 1
Download the source.

## Step 2
Change the counters array to something you want to use.
The "htmlId" must be an excisting ID in the DOM this is loaded into.

## Step 3
Change the timeLabels to what you want.

## Step 4
Include this script as seen in index.html or call it directly via your own js file.