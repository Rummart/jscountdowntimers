var Countdown = {

    "counters": null,

    "countdownTick": 0,

    /**
     * Start of the script.
     * 
     * @param {array} _counters 
     */
    "init": function(_counters, _timeLabels){

        this.counters = _counters;
        this.timeLabels = _timeLabels;

        this.countdown();
    },

    /**
     * Countdown engine.
     */
    "countdown": function(){

        this.countdownTick ++;

        for(var i = 0; i < this.counters.length; i++){

            this.counters[i].remainingTime = this.calculateRemainingTime(this.counters[i].endDate);

            var displayTime = this.getDisplayTime(this.counters[i].remainingTime);

            this.displayTime(
                this.counters[i].htmlId,
                displayTime,
                this.counters[i].label
            );

        }

        setTimeout(function(){

            Countdown.countdown();

        }, 1000);

    },

    /**
     * Calculate time remaining for each counter.
     */
    "calculateRemainingTime": function(_endDate){

        var currentDate = new Date().getTime();
        var endDateTime = new Date(_endDate).getTime();

        var remainingTime = endDateTime - currentDate;

        return remainingTime;

    },

    /**
     * Changes the timestamp to a new, fancy time. Something about human readable.
     */
    "getDisplayTime": function(_timestamp){

        // Placeholder var.
        var displayTimeString = "";

        // Convert from miliseconds to seconds, then parse the int.
        var seconds = parseInt((_timestamp / 1000), 10);

        // First display if we are past something!
        if(seconds <= 0){

            displayTimeString += " Due past!";

            return displayTimeString;

        }

        // Start converting.
        // Source: https://stackoverflow.com/a/36099084
        // Check if we are within certain time periods. So we dont display 0 years, etc.
        
        var years = Math.floor(seconds / ((3600*24)*365));

        if(years > 0){

            seconds -= years * ((3600 * 24) * 365);

            displayTimeString += years + " "+ this.getTimeLabel("years") +", ";

        }

        var days = Math.floor(seconds / (3600*24));

        if(days > 0){

            seconds -= days * 3600 * 24;
            displayTimeString += days + " "+ this.getTimeLabel("days") +", ";

        }

        var hours = Math.floor(seconds / 3600);

        if(hours > 0){

            seconds -= hours * 3600;
            displayTimeString += hours + " "+ this.getTimeLabel("hours") +", ";

        }

        var minutes = Math.floor(seconds / 60);
        
        if(minutes > 0){

            seconds  -= minutes * 60;
            displayTimeString += minutes + " "+ this.getTimeLabel("minutes") +", ";

        }
        
        displayTimeString += seconds + " "+ this.getTimeLabel("seconds") +", ";

        return displayTimeString;

    },

    /**
     * Display time function. Calls the given ID and shows the time.
     */
    "displayTime": function(_htmlId, _displayTime, _label){

        var displayString = "<strong>"+ _label +":</strong> " + _displayTime;

        document.getElementById(_htmlId).innerHTML = displayString;

    },

    /**
     * Get the label for the given timeperiod.
     */
    "getTimeLabel": function(_key){

        return this.timeLabels[_key];

    }
}